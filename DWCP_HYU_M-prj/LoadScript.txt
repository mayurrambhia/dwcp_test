﻿///$tab Header
/*---------------------------------------------------------------------------------------------------------------
   File Name 	: DWCP_Hyundai.qvw
   Description	: Visual Layer for DWCP

   Author		: Madhura M

   Project		: Dealer Website Program

   Date			: 17/03/2020
---------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------
                            Modification History

Date       Description                                         Proj#    Who	   	 	Script Change
---------- --------------------------------------------------- -------- -------- 	-------------
17/03/2020 	New Application				                        n/a      MM				Yes

---------------------------------------------------------------------------------------------------------------
**************************************************************************************************************/
///$tab Main
Binary DWCP.qvw;

SET ThousandSep=',';
SET DecimalSep='.';
SET MoneyThousandSep=',';
SET MoneyDecimalSep='.';
SET MoneyFormat='₹ #,##0.00;₹ -#,##0.00';
SET TimeFormat='hh:mm:ss';
SET DateFormat='YYYY-MM-DD';
SET TimestampFormat='YYYY-MM-DD hh:mm:ss[.fff]';
SET FirstWeekDay=6;
SET BrokenWeeks=1;
SET ReferenceDay=0;
SET FirstMonthOfYear=1;
SET CollationLocale='en-IN';
SET MonthNames='Jan;Feb;Mar;Apr;May;Jun;Jul;Aug;Sep;Oct;Nov;Dec';
SET LongMonthNames='January;February;March;April;May;June;July;August;September;October;November;December';
SET DayNames='Mon;Tue;Wed;Thu;Fri;Sat;Sun';
SET LongDayNames='Monday;Tuesday;Wednesday;Thursday;Friday;Saturday;Sunday';


/*-----------------------------------------------------------------------------------------------------------
----  #Step:1  Include configuration file........ which will have path configuration variables
------------------------------------------------------------------------------------------------------------*/
$(Include=.\Script\SLBI-Config.qvs);

//Including Variable Files
$(Must_Include=.\Script\Variables\DWCP_Variables_Monthly.txt);
$(Must_Include=.\Script\Variables\DWCP_Variables_Common.txt);


//Connect to the database
OLEDB CONNECT TO $(vDBConnectionStringQV_Directory) ;

 //Section Access for Login
Section Access;

NTNames:
LOAD ACCESS, NTNAME;
SQL
select ACCESS, replace(NTNAME, 'DB DSP\', '') as NTNAME
from [dbo].[document_access] (nolock);

IF Match(Upper(ComputerName()),'ANALYTICS') = 0 then
							//Prod Box
	Concatenate
	Load * Inline [
		ACCESS, NTNAME
		ADMIN, sls\mayur.rambhia
		ADMIN, sls\rahul.birajdar
		ADMIN, igglobal\kedar.dandekar
		ADMIN, igglobal\kasaram.vasu
		ADMIN, igglobal\jaggaraju.raju
		ADMIN, igglobal\pradeep.venkata
	];
ENDIF;

Section Application;

//As long as the SLBI-Config.qvs is present we need to keep this line (PEN test finding)
LET vDBConnectionStringQV_Directory = Null();

//---- add this variable to SLBI-Config file
vPathforHyundaiQVD ='..\data\qvd\';
vPathforHyundaiDWCPQVD ='..\data\qvd\DWCP\';

///$tab Monthly
//---------------------------------------------------------------------------------
//Drop User BQ tables (and FullVisitorID)
//---------------------------------------------------------------------------------
Drop table GA_Fact_UserDataBQ;
Drop table FullVisitorID;
//---------------------------------------------------------------------------------
//Drop GA_Fact_Leads
//---------------------------------------------------------------------------------
Drop Table GA_Fact_Leads;
//Drop table [Link Table]; Done after PCD ranking

//---------------------------------------------------------------------------------
// Create one GA_FACT (all GA except Page, Event, Site) + Page + Event + Site + BQ 
//---------------------------------------------------------------------------------
// GA Fact Leads without Page, Event, Site data
//-------------------------------------------------------------------------
GA_Lead_User_Fact:
LOAD TableName, 
     pg_ChannelGrouping, 
     pg_DeviceCategory, 
     pg_Users, 
     pg_NewUsers, 
     pg_Pageviews, 
     pg_Session, 
     pg_SessionDuration, 
     pg_Bounces, 
     ev_EventAction, 
     ev_EventCategory, 
     ev_DRSEventCategory, 
     ev_VehicleMake, 
     ev_VehicleModelNormalized, 
     ModelNormalized, 
     ev_VehicleType, 
     ev_TotalEvents, 
     ev_UniqueEvents, 
     ev_Session, 
     ev_SessionDuration, 
     ActivityKey, 
     st_ContentGroup, 
     st_PageViews, 
     EN_DealerCode, 
     EN_EnrollmentOfferProviderName, 
     EN_GoLiveDate, 
     EN_EnrollmentDate, 
//     PK_Date, 
     PK_DealerCode, 
     PK_ProviderID, 
     PK_ProviderID_DRS, 
     OEM_Code, 
     LeadDate, 
     Lead_UID, 
     Lead_HHID, 
     DummyCount, 
     Lead_Type, 
     NewCPOFlag, 
     Lead_Norm_Model, 
     Lead_Norm_Model_Qlik, 
     Lead_Model_Year, 
     SiebelDWCPChannel, 
     LeadMedium, 
     SourceTable, 
     LD_ID, 
     IsAppointmentMade, 
     %KEY_LEAD_SALE, 
     S_O, 
     Sales_Date, 
     Lead_Sale_MonthDiff, 
     Sales_Model, 
     Sales_Model_Year, 
     Dummy_Closed_Sale_Lead_UID_AllCat_Count, 
     Region_S_O, 
     District_S_O, 
     TotalLeads, 
     VehicleType, 
     OEM_Code & '_' & PK_Date_SC as OEMCode_PK_Date,
     PK_ProviderID & '|' & PK_DealerCode as PK_ProviderDealer
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Fact.qvd]
(qvd);

Concatenate

//-------------------------------------------------------------------------
// Page
//-------------------------------------------------------------------------
LOAD TableName, 
     OEM_Code, 
     OEM_Code & '_' & PK_Date as OEMCode_PK_Date, 
     PK_ProviderID, 
     PK_ProviderID_DRS, 
     PK_DealerCode, 
     pg_ChannelGrouping, 
     pg_DeviceCategory, 
     pg_Users, 
     pg_NewUsers, 
     pg_Pageviews, 
     pg_Bounces, 
     pg_Session, 
     pg_SessionDuration,
     PK_ProviderID & '|' & PK_DealerCode as PK_ProviderDealer
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Page.qvd]
(qvd);

Concatenate

//-------------------------------------------------------------------------
// Event
//-------------------------------------------------------------------------
LOAD TableName, 
     OEM_Code, 
     OEM_Code & '_' & PK_Date as OEMCode_PK_Date, 
     PK_ProviderID, 
     PK_ProviderID_DRS, 
     PK_DealerCode, 
     ev_VehicleType, 
     ev_EventAction, 
     ev_EventCategory, 
     ev_DRSEventCategory, 
     ev_VehicleMake, 
     ev_VehicleModelNormalized, 
     ModelNormalized, 
     ActivityKey, 
     ev_TotalEvents, 
     ev_UniqueEvents, 
     ev_Session, 
     ev_SessionDuration,
     PK_ProviderID & '|' & PK_DealerCode as PK_ProviderDealer
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Event.qvd]
(qvd);

Concatenate
//-------------------------------------------------------------------------
// Site
//-------------------------------------------------------------------------
LOAD TableName, 
     OEM_Code, 
     OEM_Code & '_' & PK_Date as OEMCode_PK_Date, 
     PK_ProviderID, 
     PK_ProviderID_DRS, 
     PK_DealerCode, 
     st_ContentGroup, 
     st_PageViews,
     PK_ProviderID & '|' & PK_DealerCode as PK_ProviderDealer
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Site.qvd]
(qvd);


Concatenate
//-------------------------------------------------------------------------
// User Data BQ
//-------------------------------------------------------------------------
LOAD TableName, 
     OEM_Code, 
     OEM_Code & '_' & PK_Date as OEMCode_PK_Date, 
     PK_ProviderID, 
     PK_ProviderID_DRS, 
     PK_DealerCode, 
     usbq_VisitorIDCount,
      PK_ProviderID & '|' & PK_DealerCode as PK_ProviderDealer
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_UserDataBQ.qvd]
(qvd);

NoConcatenate
DWM_TEMP:
LOAD
 Pick(Match(DWM,'Day','Week','Month'),'6 Month','13 Month','YTD') as DWM,
 OEM_Code
Resident DayWeekMonth;

Drop Table DayWeekMonth;
RENAME Table DWM_TEMP to DayWeekMonth;

Drop Fields Category, Data From DWCPMasterCalendar;
RENAME Fields Category_Monthly to Category, Data_Monthly to Data;


[Max_GA_Pull_Dates]:
LOAD 
    Max(GAPull_Date)+1 as Max_GAPull_Date
    FROM
    [$(vPathforHyundaiDWCPQVD)GA_Pull_Dates.qvd]
    (qvd);
 
LET vMaxGAPullDate = Peek('Max_GAPull_Date');
 
DROP table [Max_GA_Pull_Dates]; 

//To Handle Cross Join between Dealer & Date
Dummy:
LOAD Distinct
	PK_DealerCode,
	OEM_Code,
	PK_ProviderID
Resident GA_Lead_User_Fact;

left join(Dummy)

LOAD
	Distinct
	OEMCode_PK_Date,
	OEM_Code
Resident GA_Lead_User_Fact;

join (GA_Lead_User_Fact)
LOAD
	PK_DealerCode,
	OEM_Code,
	PK_ProviderID,
	OEMCode_PK_Date,
	'2' as Dummy
Resident Dummy;

DROP Table Dummy;
///$tab MonthlyEnrollmentFlag
//-------------------------------------------------------------------------------------------
NoConcatenate

TempEnrollmentFlag:
LOAD TableName as TableName, 
     PK_DealerCode, 
     PK_ProviderID, 
     PK_ProviderID_DRS, 
     OEM_Code,
     PK_Date_SC as PK_Date
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Fact.qvd]
(qvd)
Where Match(TableName, 'DWCP', 'DRS');

Left join
LOAD 'DWCP' as TableName, 
     OEM_Code, 
     PK_Date, 
     PK_ProviderID, 
     PK_DealerCode, 
     usbq_VisitorIDCount
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_UserDataBQ.qvd]
(qvd);

Left join
LOAD 'DWCP' as TableName,
     OEM_Code, 
     PK_Date_SC as PK_Date, 
     PK_ProviderID, 
     PK_DealerCode, 
     Count(Lead_UID) as Lead_UID
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Fact.qvd]
(qvd)
WHERE SourceTable = 'Sales Non-DRS'
Group by SourceTable, 
     OEM_Code, 
     PK_Date_SC, 
     PK_ProviderID, 
     PK_DealerCode;

//DRS
//Event table 
//ev_DRSEventCategory
LEFT JOIN
LOAD 'DRS' as TableName, 
     OEM_Code, 
     PK_Date, 
     PK_ProviderID, 
     PK_DealerCode,
     MaxString(ev_DRSEventCategory) as ev_DRSEventCategory
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Event.qvd]
(qvd)
Group by OEM_Code, 
     PK_Date, 
     PK_ProviderID,
     PK_DealerCode
;


Left join
LOAD 'DRS' as TableName,
     OEM_Code, 
     PK_Date_SC as PK_Date, 
     PK_ProviderID, 
     PK_DealerCode, 
     Count(Lead_UID) as Lead_UID_DRS
FROM
[$(vPathforHyundaiDWCPQVD)GA_M_Fact.qvd]
(qvd)
WHERE SourceTable = 'Sales DRS'
Group by SourceTable, 
     OEM_Code, 
     PK_Date_SC, 
     PK_ProviderID, 
     PK_DealerCode;

//Left join Flags in Enrollment Monthly TableNames: DWCP/DRS 
Left join (GA_Lead_User_Fact) 

LOAD TableName,
	 OEM_Code & '_' & PK_Date as OEMCode_PK_Date,
     PK_ProviderID, 
     PK_DealerCode,
     If(IsNull(usbq_VisitorIDCount), 0, 1) as GA_Flag,
	 If(IsNull(Lead_UID), 0, 1) as LD_Flag,
	 If(IsNull(ev_DRSEventCategory), 0, 1) as GA_Flag_DRS,
	 If(IsNull(Lead_UID_DRS), 0, 1) as LD_Flag_DRS

Resident TempEnrollmentFlag;

Drop Table TempEnrollmentFlag;
///$tab Additional
DateRangeList:
LOAD * INLINE [
    Date_Range_ID, Date Range
	1, Custom
	2, Today
	3, This month
	4, Week to date
	5, Month to date
	6, Year to date
	7, Last week
	8, Last month
];


ViewDataCertified:
Load *
INLINE [
ViewdataCertified,CertifiedValue
Certified,0
Certified,1

];

ViewDataOnDealersCertified:
Load *
INLINE [
OnDealersViewdataCertified,OnDealersCertifiedValue
OnDealersCertified,0
OnDealersCertified,1

];

ViewDataCertifiedbyDRS:
Load *
INLINE [
ViewdataCertifiedbyDRS,CertifiedbyDRSValue
CertifiedbyDRS,0
CertifiedbyDRS,1

];


TrafficbyChannel_Table:
Load *
INLINE [
TrafficbyChannelfield,TrafficbyChannelValue
TrafficbyChannel,0
TrafficbyChannel,1

];

SiteSection_Table:
Load *
INLINE [
SiteSectionfield,SiteSectionValue
SiteSection,0
SiteSection,1
];
TrafficbyDevice_Table:
Load *
INLINE [
TrafficbyDevicefield,TrafficbyDeviceValue
TrafficbyDevice,0
TrafficbyDevice,1

];


PageViewsLevels_Table:
Load *
INLINE [
PageViewsLevelsfield,PageViewsLevelsValue
PageViewsLevels,0
PageViewsLevels,1
];

AudienceLevels_Table:
Load *
INLINE [
AudienceLevelsfield,AudienceLevelsValue
AudienceLevels,0
AudienceLevels,1
];

VehicleType_Table:
Load *
INLINE [
VehicleTypeall,VehicleTypeall_Sort
All,1
New,2
Used,3
CPO,4
];

DDL_DrsEngagementLevels_Table:
Load *
INLINE [
Drs_EngagementLevels,Drs_EngagementLevelsFieldValue
Drs_EngagementLevels,0
Drs_EngagementLevels,1
];

//--------------------------------Provider Split----------Start-------------------------------------
Prov_rank:
Load  Distinct PK_ProviderID,
match(PK_ProviderID,'TeamVelocity',
'Sincro',
'Roadster',
'Nabthat',
'FusionZone Automotive',
'DealerOn',
'DealerFire',
'Dealer.com',
'Dealer Inspire',
'Dealer eProcess',
'CDK Global') as Sales
Resident [Link Table];

//'Nabthat',

load * inline [
PCD_PageWidth
7
];

load * inline [
PCD_PageNos
1
];

Prov_rank_DRS:
Load  Distinct PK_ProviderID_DRS,
match(PK_ProviderID_DRS,'TeamVelocity',
'Sincro',
'Roadster',
'FusionZone Automotive',
'DealerOn',
'DealerFire',
'Dealer.com',
'Dealer Inspire',
'Dealer eProcess',
'CDK Global',
'Gubagoo',
'CarNow',
'Modal') as Sales_DRS
Resident [Link Table];


load * inline [
PCD_PageWidth_DRS
7
];

load * inline [
PCD_PageNos_DRS
1
];


//--------------------------------Provider Split----------End-------------------------------------

let vVehicleType='=concat(distinct {VModel}ev_VehicleTypeAll)';

let vVehicleModel_NPrint='=concat({Model} distinct ev_VehicleModelNormalized,chr(39)&''|''&chr(39))';
let vProviderDownload='=if(Hierarchy_Level=5,''Dealer-Download'',''Download-National'')';
let vDRSDownload='=if(Hierarchy_Level=5,''DRS_Dealer_Download'',''DRS_National_Download'')';

Drop table [Link Table];

///*-----------------------------------------------------------------------------------------------------------
//----  Bundle load for images
//------------------------------------------------------------------------------------------------------------*/

ChannelGrouping:
Bundle LOAD * INLINE [
Image_ChannelGroupingID, Path
Direct, img\DWCP\Icons\ChannelGrouping\Direct.png
Display, img\DWCP\Icons\ChannelGrouping\Display.png
Email, img\DWCP\Icons\ChannelGrouping\Email.png
Organic Search, img\DWCP\Icons\ChannelGrouping\Organic Search.png
Other Advertising,img\DWCP\Icons\ChannelGrouping\Other Advertising.png
Paid Search,img\DWCP\Icons\ChannelGrouping\Paid Search.png
Referral, img\DWCP\Icons\ChannelGrouping\Referral.png
Social, img\DWCP\Icons\ChannelGrouping\Social.png
Others,img\DWCP\Icons\ChannelGrouping\Other.png
];


PrevNext:
bundle LOAD * INLINE [
PrevNext, Path
Prev,.\Images\PrevNext\Prev.png
Next,.\Images\PrevNext\Next.png
];

let NPrintingDWCPConnectionId = '=if(OEM_Code= ''HYU'', ''DWCP'', if(OEM_Code= ''GEN'', ''DWCP - GEN'', ''DWCP - HON''))';
 
let vNPrintingDWCPNSQFileName = '=if(OEM_Code= ''HYU'', ''OnDemand - DWCP.nsq'', if(OEM_Code= ''GEN'', ''OnDemand - DWCP - GEN.nsq'', ''OnDemand - DWCP - HON.nsq''))';

let NPrintingDWCPnsqFile ='=vNPrintingNSQPath & vNPrintingDWCPNSQFileName';

//For LPT Trend
LPT_AllLeadType:
LOAD * INLINE [
LPT_AllLeadType
Sales Non-DRS New Lead
Sales Non-DRS CPO Lead
Sales DRS New Lead
Sales DRS CPO Lead
Service Lead
Parts Lead
];
///$tab TBD
DROP Table DDL_LeadPerformanceTrend;
//SourceTableA,DisplayValue,LPT,OEM_Code 	
LOAD * INLINE [
    LPT,OEM_Code
    All, HYU
    Sales Non-DRS New Lead, HYU
    Sales Non-DRS CPO Lead, HYU
    Sales DRS New Lead, HYU
    Sales DRS CPO Lead, HYU
    Service Lead, HYU
    Parts Lead, HYU
    
    All, GEN
    Sales Non-DRS New Lead, GEN
    Sales Non-DRS CPO Lead, GEN
    Sales DRS New Lead, GEN
    Sales DRS CPO Lead, GEN
    Service Lead, GEN
    Parts Lead, GEN
    
    All, HON
    Sales Non-DRS New Lead, HON
    Sales Non-DRS CPO Lead, HON
    Service Lead, HON
    Parts Lead, HON
];



Temp:
Load  * inline [
Model,OEMCode
G70,GEN
G80,GEN
G90,GEN
GV70,GEN
GV80,GEN

ACCENT,HYU
ELANTRA,HYU
IONIQ,HYU
KONA,HYU
NEXO Fuel Cell,HYU
PALISADE,HYU
SANTA FE,HYU
SONATA,HYU
TUCSON,HYU
VELOSTER,HYU
VENUE,HYU

ACCORD,HON
CIVIC,HON
CLARITY,HON
CR-V,HON
FIT,HON
HR-V,HON
INSIGHT,HON
ODYSSEY,HON
PASSPORT,HON
PILOT,HON
RIDGELINE,HON




];
///$tab CRB
CRB_DimC:
LOAD * INLINE [
    DimOrder_C, OEMCodeCRB_DC, DimName_C, Group_C
    1, HYU, Region, Common
    2, HYU, ADI, Common
    3, HYU, District, Common
    4, HYU, Dealer, Common
    5, HYU, Dealer Size, Common
    6, HYU, Providers, Common
  
    1, GEN, Region, Common
    2, GEN, ADI, Common
    3, GEN, District, Common
    4, GEN, Dealer, Common
    5, GEN, Dealer Size, Common
    6, GEN, Providers, Common
  
    1, HON, Zone, Common
    2, HON, District, Common
    3, HON, Dealer, Common
    4, HON, Dealer Size, Common
    5, HON, Providers, Common
 
];

CRB_DimWM:
LOAD * INLINE [
    DimOrder_WM, DimName_WM, Group_WM
    1, Web Activity Month, Month
    
];

CRB_DimWD:
LOAD * INLINE [
    DimOrder_W, DimName_W, Group_W
    1, Web Activity Week, WeekDay
    2, Web Activity Day, WeekDay
];

CRB_DimEN:
LOAD * INLINE [
    DimOrder_N, DimName_N, Group_N
    1, Enrollment Type, Enrollment
];

CRB_DimMO:
LOAD * INLINE [
    DimOrder_M, DimName_M, Group_M
    1, Model, Model
];

CRB_DimTR:
LOAD * INLINE [
    DimOrder_T, DimName_T, Group_T
    1, Traffic Channel, Traffic
    2, Traffic Device, Traffic
];

CRB_DimSS:
LOAD * INLINE [
    DimOrder_S, DimName_S, Group_S
    1, Site Section, Site Section
];

CRB_DimEV:
LOAD * INLINE [
    DimOrder_E, DimName_E, Group_E
    1, Event Category, Event
    2, Vehicle Type, Event
    3, Browse Activity, Event
];
//    3, DRS Event Type, Event

CRB_DimLE:
LOAD * INLINE [
    DimOrder_L, DimName_L, Group_L
    1, Lead Program, Lead
];
//    1, Lead Type, Lead
//    2, Lead Program Type, Lead


CRB_Metric_OEM:
LOAD * INLINE [
    MetricID, OEMCodeCRB_M
    1, HYU
    2, HYU
    3, HYU
    4, HYU
    5, HYU
    6, HYU
    7, HYU
    8, HYU
    9, HYU
    10, HYU
    11, HYU
    12, HYU
    13, HYU
    14, HYU
    15, HYU
    16, HYU
    17, HYU
    18, HYU
    19, HYU
    20, HYU
    21, HYU
    22, HYU
    23, HYU
    24, HYU
    25, HYU
    26, HYU
    27, HYU
    28, HYU
    29, HYU
    30, HYU
    31, HYU
    32, HYU
    33, HYU
    34, HYU
    35, HYU
    36, HYU
    37, HYU
    38, HYU
    39, HYU
    40, HYU
    42, HYU
    43, HYU
    44, HYU
    45, HYU
    46, HYU
    47, HYU
    48, HYU
    49, HYU
    50, HYU
    51, HYU
    52, HYU
    53, HYU
    54, HYU
    55, HYU
    57, HYU
    58, HYU
    59, HYU
    60, HYU
    61, HYU
    62, HYU 
    63, HYU
    64, HYU
    65, HYU
    66, HYU
    1, GEN
    2, GEN
    3, GEN
    4, GEN
    5, GEN
    6, GEN
    7, GEN
    8, GEN
    9, GEN
    10, GEN
    11, GEN
    12, GEN
    13, GEN
    14, GEN
    15, GEN
    16, GEN
    17, GEN
    18, GEN
    19, GEN
    20, GEN
    21, GEN
    22, GEN
    23, GEN
    24, GEN
    25, GEN
    26, GEN
    27, GEN
    28, GEN
    29, GEN
    30, GEN
    31, GEN
    32, GEN
    33, GEN
    34, GEN
    35, GEN
    36, GEN
    37, GEN
    38, GEN
    39, GEN
    40, GEN
    42, GEN
    43, GEN
    44, GEN
    45, GEN
    46, GEN
    47, GEN
    48, GEN
    49, GEN
    50, GEN
    51, GEN
    52, GEN
    53, GEN
    54, GEN
    55, GEN
    57, GEN
    58, GEN
    59, GEN
    60, GEN
    61, GEN
    62, GEN
    63, GEN
    64, GEN
    65, GEN
    66, GEN
    2, HON
    3, HON
    4, HON
    5, HON
    6, HON
    7, HON
    8, HON
    9, HON
    10, HON
    11, HON
    12, HON
    13, HON
    14, HON
    15, HON
    16, HON
    17, HON
    18, HON
    19, HON
    20, HON
    21, HON
    22, HON
    23, HON
    24, HON
    25, HON
    26, HON
    27, HON
    28, HON
    29, HON
    30, HON
    31, HON
    32, HON
    33, HON
    34, HON
    35, HON
    36, HON
    37, HON
    38, HON
    39, HON
    40, HON
    41, HON
    42, HON
    43, HON
    44, HON
    45, HON
    46, HON
    47, HON
    48, HON
    49, HON
    50, HON
    51, HON
    52, HON
    53, HON
    54, HON
    55, HON
    56, HON
    57, HON
    58, HON
    59, HON
    60, HON
    61, HON
    62, HON
    63, HON
    64, HON
    65, HON
    66, HON
];

LOAD * INLINE [
    MetricID, MetricOrder, MetricGroup, MetricName, MetricSource, Group_C,Group_WM, Group_T, Group_S, Group_BQ, Group_L, Group_E, Group_N, Group_M, Group_W
    1, 1, Website Measure, Appointment Conversion Rate, Leads, Common,Month, , , , , , , , WeekDay
    2, 2, Website Measure, Average Time On Site, Page, Common,Month, Traffic, , , , , , , WeekDay
    3, 3, Website Measure, Bounce Rate, Page, Common,Month, Traffic, , , , , , , WeekDay
    4, 4, Website Measure, Enrollment Dealer%, Leads, Common,Month, , , , , , Enrollment
    5, 5, Website Measure, Enrollment Dealer Count, Leads, Common,Month, , , , , , Enrollment
    6, 6, Website Measure, New Users, Page, Common,Month, Traffic, , , , , , , WeekDay
    7, 7, Website Measure, New VDP by Model, Event, Common,Month, , , , , , , Model, WeekDay
    8, 8, Website Measure, Pageviews, Site, Common,Month, , Site Section, , , , , , WeekDay
    9, 9, Website Measure, Pageviews per User, Page, Common,Month, , , , , , , , WeekDay
    10, 10, Website Measure, Pageviews per Visit, Site, Common,Month, , , , , , , , WeekDay
    11, 11, Website Measure, Returning Users, Page, Common,Month, Traffic, , , , , , , WeekDay
    12, 12, Website Measure, Total Events, Event, Common,Month, , , , , Event, , Model, WeekDay
    13, 13, Website Measure, Unique Events, Event, Common,Month, , , , , Event, , Model, WeekDay
    14, 14, Website Measure, Unique Users, BQ, Common,Month, , , UniqueUser, , , , , WeekDay
    15, 15, Website Measure, Website Unique Users, BQ, Common,Month, , , UniqueUser, , , , , WeekDay
    16, 16, DRS Measure, DRS Unique Users, Leads, Common,Month, , , , , , , , WeekDay
    17, 17, Website Measure, VDP Pageviews, Site, Common,Month, , Site Section, , , , , , WeekDay
    18, 18, Website Measure, VDP Pageviews per Visit, Site, Common,Month, , , , , , , , WeekDay
    19, 19, Website Measure, VDP Pageviews per Lead, Site, Common,Month, , , , , , , , WeekDay
    20, 20, Website Measure, VDP Pageviews per User, Site, Common,Month, , , , , , , , WeekDay
    21, 21, Website Measure, Visits, Page, Common,Month, Traffic, , , , , , , WeekDay
    22, 22, Website Measure, VLP Pageviews, Site, Common,Month, , Site Section, , , , , , WeekDay
    23, 23, Website Measure, VLP Pageviews per Visit, Site, Common,Month, , , , , , , , WeekDay
    24, 24, Website Measure, VLP Pageviews per User, Site, Common,Month, , , , , , , , WeekDay
    25, 25, DRS Measure, Abandonment Rate, Event, Common,Month, , , , , , , , WeekDay
    26, 26, DRS Measure, Average Time on DRS, Event, Common,Month, , , , , , , , WeekDay
    27, 27, DRS Measure, Credit Applications, Event, Common,Month, , , , , , , , WeekDay
    28, 28, DRS Measure, DRS Engagement Rate, Event, Common,Month, , , , , , , , WeekDay
    29, 29, DRS Measure, DRS Lead Conversion Rate, Leads, Common,Month, , , , , , , , WeekDay
    30, 30, DRS Measure, DRS Unique Start, Leads, Common,Month, , , , , , , , WeekDay
    31, 31, DRS Measure, Payment Calculations, Event, Common,Month, , , , , , , , WeekDay
    32, 32, DRS Measure, Test Drive, Event, Common,Month, , , , , , , , WeekDay
    33, 33, DRS Measure, Trade Evaluations, Event, Common,Month, , , , , , , , WeekDay
    34, 34, Lead Measure, CPO Total Sales Leads, Leads, Common,Month, , , , Lead, , , Model, WeekDay
    35, 35, Lead Measure, CPO Unique Sales Leads, Leads, Common,Month, , , , Lead, , , Model, WeekDay  
    36, 36, Lead Measure, CPO DRS Leads, Leads, Common,Month, , , , Lead, , , , WeekDay
    37, 37, Lead Measure, CPO Sales, Leads, Common,Month, , , , Lead, , , , WeekDay
    38, 38, Lead Measure, CPO Lost Sales, Leads, Common,Month, , , , Lead, , , WeekDay
    39, 39, Lead Measure, CPO Sales TCR, Leads, Common,Month, , , , Lead, , , , WeekDay
    40, 40, Lead Measure, CPO Sales UCR, Leads, Common,Month, , , , Lead, , , , WeekDay 
    41, 41, DRS Measure, CPO DRS Sales, Event, Common,Month, , , , , , , , WeekDay 
    42, 42, Lead Measure, CPO 30Day Sales (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    43, 43, Lead Measure, CPO 30Day UCR (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    44, 44, Lead Measure, CPO 60Day Sales (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    45, 45, Lead Measure, CPO 60Day UCR (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    46, 46, Lead Measure, CPO 90Day Sales (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    47, 47, Lead Measure, CPO 90Day UCR (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    48, 48, Lead Measure, Lead Conversion Rate, Leads, Common,Month, , , , , , , , WeekDay
    49, 49, Lead Measure, New Total Sales Leads, Leads, Common,Month, , , , Lead, , , Model, WeekDay
    50, 50, Lead Measure, New Unique Sales Leads, Leads, Common,Month, , , , Lead, , , Model, WeekDay
    51, 51, Lead Measure, New DRS Leads, Leads, Common,Month, , , , Lead, , , , WeekDay
    52, 52, Lead Measure, New Sales, Leads, Common,Month, , , , Lead, , , , WeekDay
    53, 53, Lead Measure, New Lost Sales, Leads, Common,Month, , , , Lead, , , WeekDay
    54, 54, Lead Measure, New Sales TCR, Leads, Common,Month, , , , Lead, , , , WeekDay
    55, 55, Lead Measure, New Sales UCR, Leads, Common,Month, , , , Lead, , , , WeekDay
    56, 56, DRS Measure, New DRS Sales, Event, Common,Month, , , , , , , , WeekDay
    57, 57, Lead Measure, New 30Day Sales (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    58, 58, Lead Measure, New 30Day UCR (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    59, 59, Lead Measure, New 60Day Sales (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    60, 60, Lead Measure, New 60Day UCR (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    61, 61, Lead Measure, New 90Day Sales (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    62, 62, Lead Measure, New 90Day UCR (Use with Web Activity Month), Leads, Common,Month, , , , Lead, , , , WeekDay
    63, 63, Lead Measure, Parts Total Leads, Leads, Common,Month, , , , Lead, , , , WeekDay
    64, 64, Lead Measure, Parts Unique Leads, Leads, Common,Month, , , , Lead, , , , WeekDay
    65, 65, Lead Measure, Service Total Leads, Leads, Common,Month, , , , Lead, , , , WeekDay
    66, 66, Lead Measure, Service Unique Leads, Leads, Common,Month, , , , Lead, , , , WeekDay
];
///$tab Sheets
Sheets:
LOAD * Inline [
SheetName
Dashboard
SH_TrafficAcquisition
SH_Engagement
SH_WebProviderDashboard
SH_CRB
SH_Landing
];